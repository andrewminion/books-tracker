<?php

use App\Http\Livewire\Book\Single as BookSingle;
use App\Http\Livewire\Series\Index as SeriesIndex;
use App\Http\Livewire\Series\Single as SeriesSingle;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::middleware(['auth:sanctum', 'verified'])->group(function () {
    Route::get('/dashboard', function () {
        return view('dashboard');
    })->name('dashboard');

    Route::get('/goals', function () {
        return view('goals');
    })->name('goals');

    Route::get('/series', SeriesIndex::class)->name('series.index');
    Route::get('/series/{id}', SeriesSingle::class)->name('series.show');

    Route::get('/book/{bookId}', BookSingle::class)->name('book.show');
});
