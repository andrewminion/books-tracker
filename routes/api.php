<?php

use Illuminate\Console\Command;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware(['auth:sanctum'])
    ->post('deploy-assets', function (Request $request) {
        abort_unless($request->user()->tokenCan('update'), 403);

        $sucessful = Artisan::call('airdrop:download') === Command::SUCCESS;

        return response()->json(status: $sucessful ? 200 : 400);
    })
    ->name('deploy-assets');
