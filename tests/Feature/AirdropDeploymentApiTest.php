<?php

namespace Tests\Feature;

use Illuminate\Support\Facades\Storage;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class AirdropDeploymentApiTest extends TestCase
{
    public function test_endpoint_requires_authentication()
    {
        $this
            ->postJson(route('deploy-assets'))
            ->assertUnauthorized();
    }

    public function test_endpoint_requires_authorization()
    {
        Sanctum::actingAs($this->user(), ['create']);

        $this
            ->postJson(route('deploy-assets'))
            ->assertForbidden();
    }

    public function test_endpoint_works_for_authorized_user()
    {
        Sanctum::actingAs($this->user(), ['update']);

        Storage::fake('airdrop');

        $this
            ->postJson(route('deploy-assets'))
            ->assertOk();
    }
}
