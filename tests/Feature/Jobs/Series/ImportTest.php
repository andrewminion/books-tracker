<?php

namespace Tests\Feature\Jobs\Series;

use App\Exceptions\SeriesImportFailedException;
use App\Imports\Books\BookSeriesInOrder;
use App\Imports\Books\TheStoryGraph;
use App\Jobs\Series\Import;
use Tests\TestCase;

class ImportTest extends TestCase
{
    public function test_invalid_url_throws_exception()
    {
        $this->expectException(SeriesImportFailedException::class);

        new Import('https://example.com');
    }

    public function test_bookseriesinorder_runs_correct_job()
    {
        $job = new Import('https://bookseriesinorder.com/');

        $this->assertInstanceOf(BookSeriesInOrder::class, invade($job)->import); // @phpstan-ignore-line
    }

    public function test_storygraph_runs_correct_job()
    {
        $job = new Import('https://app.thestorygraph.com/series/123');

        $this->assertInstanceOf(TheStoryGraph::class, invade($job)->import); // @phpstan-ignore-line
    }
}
