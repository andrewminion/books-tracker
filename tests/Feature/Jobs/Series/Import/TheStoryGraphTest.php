<?php

namespace Tests\Feature\Jobs\Series\Import;

use App\Imports\Books\TheStoryGraph;
use App\Models\Book;
use App\Models\Series;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Tests\TestCase;

class TheStoryGraphTest extends TestCase
{
    public static function tearDownAfterClass(): void
    {
        Str::createRandomStringsNormally();

        parent::tearDownAfterClass();
    }

    public function test_import_works()
    {
        $this->assertDatabaseCount(Book::class, 0);
        $this->assertDatabaseCount(Series::class, 0);

        Storage::fake();
        Str::createRandomStringsUsingSequence(collect(range(1, 10))->map(fn ($index) => 'random-'.$index)->toArray());

        (new TheStoryGraph('https://app.thestorygraph.com/series/112204'))->handle();

        $this->assertDatabaseHas(Series::class, [
            'name' => 'Lady Astronaut',
        ]);

        /** @var Series $series */
        $series = Series::where('name', 'Lady Astronaut')->with('books')->first();

        $this->assertDatabaseHas(Book::class, [
            'title' => 'We Interrupt This Broadcast',
            'published_at' => '2013-01-01',
            'cover_image' => 'covers/random-1',
        ]);
        $this->assertEquals(1, $series->books->firstWhere('title', 'We Interrupt This Broadcast')->pivot->sort_order);
        $this->assertEquals(0.5, $series->books->firstWhere('title', 'We Interrupt This Broadcast')->pivot->display_order);
        Storage::assertExists('public/covers/random-1');

        $this->assertDatabaseHas(Book::class, [
            'title' => 'The Calculating Stars',
            'published_at' => '2018-01-01',
            'cover_image' => 'covers/random-2',
        ]);
        $this->assertEquals(2, $series->books->firstWhere('title', 'The Calculating Stars')->pivot->sort_order);
        $this->assertEquals(1, $series->books->firstWhere('title', 'The Calculating Stars')->pivot->display_order);
        Storage::assertExists('public/covers/random-2');

        $this->assertDatabaseHas(Book::class, [
            'title' => 'Articulated Restraint',
            'published_at' => '2019-01-01',
            'cover_image' => 'covers/random-3',
        ]);
        $this->assertEquals(3, $series->books->firstWhere('title', 'Articulated Restraint')->pivot->sort_order);
        $this->assertEquals(1.5, $series->books->firstWhere('title', 'Articulated Restraint')->pivot->display_order);
        Storage::assertExists('public/covers/random-3');

        $this->assertDatabaseHas(Book::class, [
            'title' => 'The Fated Sky',
            'published_at' => '2018-01-01',
            'cover_image' => 'covers/random-4',
        ]);
        $this->assertEquals(4, $series->books->firstWhere('title', 'The Fated Sky')->pivot->sort_order);
        $this->assertEquals(2, $series->books->firstWhere('title', 'The Fated Sky')->pivot->display_order);
        Storage::assertExists('public/covers/random-4');

        $this->assertDatabaseHas(Book::class, [
            'title' => 'The Relentless Moon',
            'published_at' => '2020-01-01',
            'cover_image' => 'covers/random-5',
        ]);
        $this->assertEquals(5, $series->books->firstWhere('title', 'The Relentless Moon')->pivot->sort_order);
        $this->assertEquals(3, $series->books->firstWhere('title', 'The Relentless Moon')->pivot->display_order);
        Storage::assertExists('public/covers/random-5');

        $this->assertDatabaseHas(Book::class, [
            'title' => 'The Martian Contingency',
            'published_at' => '2022-01-01',
            'cover_image' => null,
        ]);
        $this->assertEquals(6, $series->books->firstWhere('title', 'The Martian Contingency')->pivot->sort_order);
        $this->assertEquals(4, $series->books->firstWhere('title', 'The Martian Contingency')->pivot->display_order);

        $this->assertDatabaseHas(Book::class, [
            'title' => 'The Phobos Experience',
            'published_at' => '2018-01-01',
            'cover_image' => null,
        ]);
        $this->assertEquals(7, $series->books->firstWhere('title', 'The Phobos Experience')->pivot->sort_order);
        $this->assertEquals(4.1, $series->books->firstWhere('title', 'The Phobos Experience')->pivot->display_order);

        $this->assertDatabaseHas(Book::class, [
            'title' => 'Amara\'s Giraffe',
            'published_at' => '2018-01-01',
            'cover_image' => 'covers/random-6',
        ]);
        $this->assertEquals(8, $series->books->firstWhere('title', 'Amara\'s Giraffe')->pivot->sort_order);
        $this->assertEquals(4.3, $series->books->firstWhere('title', 'Amara\'s Giraffe')->pivot->display_order);
        Storage::assertExists('public/covers/random-6');

        $this->assertDatabaseHas(Book::class, [
            'title' => 'Rocket\'s Red',
            'published_at' => '2015-01-01',
            'cover_image' => 'covers/random-7',
        ]);
        $this->assertEquals(9, $series->books->firstWhere('title', 'Rocket\'s Red')->pivot->sort_order);
        $this->assertEquals(4.4, $series->books->firstWhere('title', 'Rocket\'s Red')->pivot->display_order);
        Storage::assertExists('public/covers/random-7');

        $this->assertDatabaseHas(Book::class, [
            'title' => 'The Lady Astronaut of Mars',
            'published_at' => '2012-01-01',
            'cover_image' => 'covers/random-8',
        ]);
        $this->assertEquals(10, $series->books->firstWhere('title', 'The Lady Astronaut of Mars')->pivot->sort_order);
        $this->assertEquals(4.5, $series->books->firstWhere('title', 'The Lady Astronaut of Mars')->pivot->display_order);
        Storage::assertExists('public/covers/random-8');
    }
}
