<?php

namespace Tests\Feature\Jobs\Series\Import;

use App\Imports\Books\BookSeriesInOrder;
use App\Models\Book;
use App\Models\Series;
use Tests\TestCase;

class BookSeriesInOrderTest extends TestCase
{
    public function test_import_works()
    {
        $this->assertDatabaseCount(Book::class, 0);
        $this->assertDatabaseCount(Series::class, 0);

        (new BookSeriesInOrder('https://www.bookseriesinorder.com/jack-ryan-john-clark/'))->import();

        $this->assertDatabaseHas(Series::class, [
            'name' => 'Jack Ryan & John Clark',
        ]);

        /** @var Series $series */
        $series = Series::where('name', 'Jack Ryan & John Clark')->with('books')->first();

        $this->assertDatabaseHas(Book::class, [
            'title' => 'The Hunt for Red October(By: Tom Clancy)',
            'published_at' => '1984-01-01',
        ]);
        $this->assertEquals(1, $series->books->firstWhere('title', 'The Hunt for Red October(By: Tom Clancy)')->pivot->sort_order);

        $this->assertDatabaseHas(Book::class, [
            'title' => 'Patriot Games(By: Tom Clancy)',
            'published_at' => '1987-01-01',
        ]);
        $this->assertEquals(2, $series->books->firstWhere('title', 'Patriot Games(By: Tom Clancy)')->pivot->sort_order);

        $this->assertDatabaseHas(Book::class, [
            'title' => 'The Cardinal of the Kremlin(By: Tom Clancy)',
            'published_at' => '1988-01-01',
        ]);
        $this->assertEquals(3, $series->books->firstWhere('title', 'The Cardinal of the Kremlin(By: Tom Clancy)')->pivot->sort_order);

        $this->assertDatabaseHas(Book::class, [
            'title' => 'Clear and Present Danger(By: Tom Clancy)',
            'published_at' => '1989-01-01',
        ]);
        $this->assertEquals(4, $series->books->firstWhere('title', 'Clear and Present Danger(By: Tom Clancy)')->pivot->sort_order);

        $this->assertDatabaseHas(Book::class, [
            'title' => 'The Sum of All Fears(By: Tom Clancy)',
            'published_at' => '1991-01-01',
        ]);
        $this->assertEquals(5, $series->books->firstWhere('title', 'The Sum of All Fears(By: Tom Clancy)')->pivot->sort_order);

        $this->assertDatabaseHas(Book::class, [
            'title' => 'Without Remorse(By: Tom Clancy)',
            'published_at' => '1993-01-01',
        ]);
        $this->assertEquals(6, $series->books->firstWhere('title', 'Without Remorse(By: Tom Clancy)')->pivot->sort_order);

        $this->assertDatabaseHas(Book::class, [
            'title' => 'Debt of Honor(By: Tom Clancy)',
            'published_at' => '1994-01-01',
        ]);
        $this->assertEquals(7, $series->books->firstWhere('title', 'Debt of Honor(By: Tom Clancy)')->pivot->sort_order);

        $this->assertDatabaseHas(Book::class, [
            'title' => 'Executive Orders(By: Tom Clancy)',
            'published_at' => '1996-01-01',
        ]);
        $this->assertEquals(8, $series->books->firstWhere('title', 'Executive Orders(By: Tom Clancy)')->pivot->sort_order);

        $this->assertDatabaseHas(Book::class, [
            'title' => 'Rainbow Six(By: Tom Clancy)',
            'published_at' => '1998-01-01',
        ]);
        $this->assertEquals(9, $series->books->firstWhere('title', 'Rainbow Six(By: Tom Clancy)')->pivot->sort_order);

        $this->assertDatabaseHas(Book::class, [
            'title' => 'The Bear and the Dragon(By: Tom Clancy)',
            'published_at' => '2000-01-01',
        ]);
        $this->assertEquals(10, $series->books->firstWhere('title', 'The Bear and the Dragon(By: Tom Clancy)')->pivot->sort_order);

        $this->assertDatabaseHas(Book::class, [
            'title' => 'Red Rabbit(By: Tom Clancy)',
            'published_at' => '2002-01-01',
        ]);
        $this->assertEquals(11, $series->books->firstWhere('title', 'Red Rabbit(By: Tom Clancy)')->pivot->sort_order);

        $this->assertDatabaseHas(Book::class, [
            'title' => 'The Teeth of the Tiger(By: Tom Clancy)',
            'published_at' => '2003-01-01',
        ]);
        $this->assertEquals(12, $series->books->firstWhere('title', 'The Teeth of the Tiger(By: Tom Clancy)')->pivot->sort_order);

        $this->assertDatabaseHas(Book::class, [
            'title' => 'Dead or Alive(By: Tom Clancy,Grant Blackwood)',
            'published_at' => '2010-01-01',
        ]);
        $this->assertEquals(13, $series->books->firstWhere('title', 'Dead or Alive(By: Tom Clancy,Grant Blackwood)')->pivot->sort_order);

        $this->assertDatabaseHas(Book::class, [
            'title' => 'Locked On(By: Mark Greaney,Tom Clancy)',
            'published_at' => '2011-01-01',
        ]);
        $this->assertEquals(14, $series->books->firstWhere('title', 'Locked On(By: Mark Greaney,Tom Clancy)')->pivot->sort_order);

        $this->assertDatabaseHas(Book::class, [
            'title' => 'Threat Vector(By: Mark Greaney,Tom Clancy)',
            'published_at' => '2012-01-01',
        ]);
        $this->assertEquals(15, $series->books->firstWhere('title', 'Threat Vector(By: Mark Greaney,Tom Clancy)')->pivot->sort_order);

        $this->assertDatabaseHas(Book::class, [
            'title' => 'Command Authority(By: Mark Greaney,Tom Clancy)',
            'published_at' => '2013-01-01',
        ]);
        $this->assertEquals(16, $series->books->firstWhere('title', 'Command Authority(By: Mark Greaney,Tom Clancy)')->pivot->sort_order);

        $this->assertDatabaseHas(Book::class, [
            'title' => 'Support and Defend(By:Mark Greaney)',
            'published_at' => '2014-01-01',
        ]);
        $this->assertEquals(17, $series->books->firstWhere('title', 'Support and Defend(By:Mark Greaney)')->pivot->sort_order);

        $this->assertDatabaseHas(Book::class, [
            'title' => 'Full Force and Effect(By:Mark Greaney)',
            'published_at' => '2014-01-01',
        ]);
        $this->assertEquals(18, $series->books->firstWhere('title', 'Full Force and Effect(By:Mark Greaney)')->pivot->sort_order);

        $this->assertDatabaseHas(Book::class, [
            'title' => 'Under Fire(By:Grant Blackwood)',
            'published_at' => '2015-01-01',
        ]);
        $this->assertEquals(19, $series->books->firstWhere('title', 'Under Fire(By:Grant Blackwood)')->pivot->sort_order);

        $this->assertDatabaseHas(Book::class, [
            'title' => 'Commander-in-Chief(By:Mark Greaney)',
            'published_at' => '2015-01-01',
        ]);
        $this->assertEquals(20, $series->books->firstWhere('title', 'Commander-in-Chief(By:Mark Greaney)')->pivot->sort_order);

        $this->assertDatabaseHas(Book::class, [
            'title' => 'Duty and Honor(By:Grant Blackwood)',
            'published_at' => '2016-01-01',
        ]);
        $this->assertEquals(21, $series->books->firstWhere('title', 'Duty and Honor(By:Grant Blackwood)')->pivot->sort_order);

        $this->assertDatabaseHas(Book::class, [
            'title' => 'True Faith and Allegiance(By:Mark Greaney)',
            'published_at' => '2016-01-01',
        ]);
        $this->assertEquals(22, $series->books->firstWhere('title', 'True Faith and Allegiance(By:Mark Greaney)')->pivot->sort_order);

        $this->assertDatabaseHas(Book::class, [
            'title' => 'Point of Contact(By:Mike Maden)',
            'published_at' => '2017-01-01',
        ]);
        $this->assertEquals(23, $series->books->firstWhere('title', 'Point of Contact(By:Mike Maden)')->pivot->sort_order);

        $this->assertDatabaseHas(Book::class, [
            'title' => 'Power and Empire(By:Marc Cameron)',
            'published_at' => '2017-01-01',
        ]);
        $this->assertEquals(24, $series->books->firstWhere('title', 'Power and Empire(By:Marc Cameron)')->pivot->sort_order);

        $this->assertDatabaseHas(Book::class, [
            'title' => 'Line of Sight(By:Mike Maden)',
            'published_at' => '2018-01-01',
        ]);
        $this->assertEquals(25, $series->books->firstWhere('title', 'Line of Sight(By:Mike Maden)')->pivot->sort_order);

        $this->assertDatabaseHas(Book::class, [
            'title' => 'Oath of Office(By:Marc Cameron)',
            'published_at' => '2018-01-01',
        ]);
        $this->assertEquals(26, $series->books->firstWhere('title', 'Oath of Office(By:Marc Cameron)')->pivot->sort_order);

        $this->assertDatabaseHas(Book::class, [
            'title' => 'Enemy Contact(By:Mike Maden)',
            'published_at' => '2019-01-01',
        ]);
        $this->assertEquals(27, $series->books->firstWhere('title', 'Enemy Contact(By:Mike Maden)')->pivot->sort_order);

        $this->assertDatabaseHas(Book::class, [
            'title' => 'Code of Honor(By:Marc Cameron)',
            'published_at' => '2019-01-01',
        ]);
        $this->assertEquals(28, $series->books->firstWhere('title', 'Code of Honor(By:Marc Cameron)')->pivot->sort_order);

        $this->assertDatabaseHas(Book::class, [
            'title' => 'Firing Point(By:Mike Maden)',
            'published_at' => '2020-01-01',
        ]);
        $this->assertEquals(29, $series->books->firstWhere('title', 'Firing Point(By:Mike Maden)')->pivot->sort_order);

        $this->assertDatabaseHas(Book::class, [
            'title' => 'Shadow of the Dragon(By:Marc Cameron)',
            'published_at' => '2020-01-01',
        ]);
        $this->assertEquals(30, $series->books->firstWhere('title', 'Shadow of the Dragon(By:Marc Cameron)')->pivot->sort_order);

        $this->assertDatabaseHas(Book::class, [
            'title' => 'Target Acquired(By:Don Bentley)',
            'published_at' => '2021-01-01',
        ]);
        $this->assertEquals(31, $series->books->firstWhere('title', 'Target Acquired(By:Don Bentley)')->pivot->sort_order);

        $this->assertDatabaseHas(Book::class, [
            'title' => 'Chain of Command(By: Tom Clancy,Marc Cameron)',
            'published_at' => '2021-01-01',
        ]);
        $this->assertEquals(32, $series->books->firstWhere('title', 'Chain of Command(By: Tom Clancy,Marc Cameron)')->pivot->sort_order);

        $this->assertDatabaseHas(Book::class, [
            'title' => 'Zero Hour(By:Don Bentley)',
            'published_at' => '2022-01-01',
        ]);
        $this->assertEquals(33, $series->books->firstWhere('title', 'Zero Hour(By:Don Bentley)')->pivot->sort_order);
    }
}
