<?php

namespace Tests\Feature;

use App\Models\Goal;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class GoalTimeMutatorsTest extends TestCase
{
    use RefreshDatabase;

    public function test_percentage_for_pages()
    {
        $goal = Goal::factory()->create();

        $goal->progress_pages = $goal->pages / 2;
        $goal->save();
        $this->assertEquals(.5, $goal->fresh()->percentage);

        $goal->progress_pages = $goal->pages / 3;
        $goal->save();
        $this->assertEquals(.33, $goal->fresh()->percentage);

        $goal->progress_pages = $goal->pages / 1.2;
        $goal->save();
        $this->assertEquals(.83, $goal->fresh()->percentage);
    }

    public function test_percentage_for_time()
    {
        $goal = Goal::factory()->create([
            'unit' => 'time',
            'time' => '2:00:00',
        ]);

        $goal->progress_time = '0:30:00';
        $goal->save();
        $this->assertEquals(.25, $goal->fresh()->percentage);

        $goal->progress_time = '1:20:00';
        $goal->save();
        $this->assertEquals(.67, $goal->fresh()->percentage);

        $goal->progress_time = '2:00:00';
        $goal->save();
        $this->assertEquals(1.00, $goal->fresh()->percentage);
    }
}
