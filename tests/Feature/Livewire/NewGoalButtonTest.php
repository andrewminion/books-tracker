<?php

namespace Tests\Feature\Livewire;

use App\Http\Livewire\NewGoalButton;
use App\Models\Goal;
use Illuminate\Support\Facades\Auth;
use Livewire\Livewire;
use Tests\TestCase;

class NewGoalButtonTest extends TestCase
{
    public function test_the_component_can_render()
    {
        $user = $this->user();
        Auth::login($user);

        $this->assertDatabaseCount(Goal::class, 0);

        Livewire::test(NewGoalButton::class)
            ->call('createGoal')
            ->assertRedirect(route('goals'));

        $this->assertDatabaseHas(Goal::class, [
            'user_id' => $user->id,
            'name' => 'New Goal',
        ]);
    }
}
