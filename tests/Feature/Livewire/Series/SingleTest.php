<?php

namespace Tests\Feature\Livewire\Series;

use App\Enums\CompletionStatusEnum;
use App\Http\Livewire\Series\Single;
use App\Models\Book;
use App\Models\Series;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Livewire\Livewire;
use Tests\TestCase;

class SingleTest extends TestCase
{
    public function test_the_component_can_render()
    {
        /** @var User */
        $user = User::factory()->create();

        Auth::login($user);

        /** @var Series */
        $series = Series::factory()
            ->hasAttached(Book::factory()->count(3))
            ->create();

        $user->completed_books()->attach($series->books[0]);
        $user->completed_books()->attach($series->books[1], [
            'characters' => 1,
            'atmosphere' => 2,
            'writing_style' => 3,
            'plot' => 4,
            'intrigue' => 5,
            'logic' => 6,
            'enjoyment' => 7,
        ]);

        $component = Livewire::test(Single::class, ['id' => $series->id])
            ->assertSeeInOrder([
                $series->books[0]->title,
                'Published '.$series->books[0]->published_at->format('Y'),
                route('book.show', ['bookId' => $series->books[0]->id]),
                'w-6 h-6 text-green-600', // Completed.

                $series->books[1]->title,
                'Published '.$series->books[1]->published_at->format('Y'),
                'rating',
                route('book.show', ['bookId' => $series->books[1]->id]),
                'w-6 h-6 text-green-600', // Completed.

                $series->books[2]->title,
                'Published '.$series->books[2]->published_at->format('Y'),
                route('book.show', ['bookId' => $series->books[2]->id]),
                'w-6 h-6 text-gray-400', // Not complete.
            ], false);

        $component->assertStatus(200);
    }

    public function test_the_component_toggles_dnf_and_completed()
    {
        /** @var User */
        $user = User::factory()->create();

        Auth::login($user);

        /** @var Series */
        $series = Series::factory()
            ->hasAttached(Book::factory()->count(3))
            ->create();

        $user->completed_books()->attach($series->books[0]);
        $user->dnf_books()->attach($series->books[1]);

        $this->assertDatabaseHas('book_user', [
            'book_id' => $series->books[1]->id,
            'user_id' => $user->id,
            'status' => CompletionStatusEnum::dnf(),
        ]);

        $component = Livewire::test(Single::class, ['id' => $series->id])
            ->assertSeeInOrder([
                $series->books[0]->title,
                'Published '.$series->books[0]->published_at->format('Y'),
                'w-6 h-6 text-green-600', // Completed.

                $series->books[1]->title,
                'Published '.$series->books[1]->published_at->format('Y'),
                'w-6 h-6 text-yellow-600', // DNF.

                $series->books[2]->title,
                'Published '.$series->books[2]->published_at->format('Y'),
                'w-6 h-6 text-gray-400', // Not complete.
            ], false);

        // Change DNF to complete.
        $component
            ->call('toggleComplete', $series->books[1]->id)
            ->assertHasNoErrors();

        $this->assertDatabaseHas('book_user', [
            'book_id' => $series->books[1]->id,
            'user_id' => $user->id,
            'status' => CompletionStatusEnum::read(),
        ]);

        // Change complete to DNF.
        $component
            ->call('toggleDnf', $series->books[0]->id)
            ->assertHasNoErrors();

        $this->assertDatabaseHas('book_user', [
            'book_id' => $series->books[0]->id,
            'user_id' => $user->id,
            'status' => CompletionStatusEnum::dnf(),
        ]);

        // Change none to completed.
        $component
            ->call('toggleComplete', $series->books[2]->id)
            ->assertHasNoErrors();

        $this->assertDatabaseHas('book_user', [
            'book_id' => $series->books[2]->id,
            'user_id' => $user->id,
            'status' => CompletionStatusEnum::read(),
        ]);

        $component->assertStatus(200);
    }
}
