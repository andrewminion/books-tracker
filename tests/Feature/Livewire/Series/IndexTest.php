<?php

namespace Tests\Feature\Livewire\Series;

use App\Http\Livewire\Series\Index;
use App\Models\Series;
use App\Models\User;
use Laravel\Sanctum\Sanctum;
use Livewire\Livewire;
use Maatwebsite\Excel\Facades\Excel;
use Tests\TestCase;

class IndexTest extends TestCase
{
    public function test_the_component_can_render()
    {
        Sanctum::actingAs(User::factory()->create());

        Livewire::test(Index::class)
            ->assertStatus(200);
    }

    public function test_the_component_shows_data_and_actions_work()
    {
        $user = User::factory()->create();
        Sanctum::actingAs($user);

        /**
         * @var Series $series1
         * @var Series $series2
         */
        [$series1, $series2] = Series::factory()->times(2)->sequence(
            ['name' => 'Series 1'],
            ['name' => 'Series 2'],
        )->create();

        $component = Livewire::test(Index::class)
            ->assertSeeInOrder([
                'Series 1', 'group-hover:text-gray-500', // From unchecked checkmark.
                'Series 2', 'group-hover:text-gray-500', // From unchecked checkmark.
            ])

            ->call('toggleMine', $series1->id)
            ->assertHasNoErrors();

        $this->assertDatabaseHas('series_user', [
            'series_id' => $series1->id,
            'user_id' => $user->id,
        ]);

        $this->assertTrue($user->fresh()->series->contains('id', $series1->id));

        Sanctum::actingAs($user->fresh());

        Livewire::test(Index::class)
            ->assertSeeInOrder([
                'Series 1', 'text-red-600', // From checked checkmark.
                'Series 2', 'group-hover:text-gray-500', // From unchecked checkmark.
            ]);
    }

    public function test_the_component_imports_series()
    {
        $user = User::factory()->create();
        Sanctum::actingAs($user);

        Excel::fake();

        $this->assertDatabaseCount(Series::class, 0);

        Livewire::test(Index::class)
            ->assertSeeHtml('Add a series')
            ->set('newSeries', 'https://www.bookseriesinorder.com/jack-ryan-john-clark/')
            ->call('addSeries')
            ->assertHasNoErrors()
            ->assertSet('newSeries', '');

        $localPath = storage_path('app/tmp/'.hash('sha256', 'https://www.bookseriesinorder.com/jack-ryan-john-clark/').'.html');
        Excel::assertImported($localPath);
    }
}
