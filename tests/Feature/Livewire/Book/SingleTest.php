<?php

namespace Tests\Feature\Livewire\Book;

use App\Http\Livewire\Book\Single;
use App\Models\Book;
use Illuminate\Support\Facades\Auth;
use Livewire\Livewire;
use Tests\TestCase;

class SingleTest extends TestCase
{
    public function test_the_component_can_render()
    {
        $book = Book::factory()->create();

        Livewire::test(Single::class, ['bookId' => $book->id])
            ->assertHasNoErrors()
            ->assertSeeInOrder([
                $book->title,
                $book->description,
            ])
            ->assertDontSee('Review');

        Auth::login($this->user());

        Livewire::test(Single::class, ['bookId' => $book->id])
            ->assertHasNoErrors()
            ->assertSeeInOrder([
                $book->title,
                $book->description,
                'Review',
            ]);
    }

    public function test_component_fills_properties_from_existing_review()
    {
        $user = $this->user();
        $book = Book::factory()->create();
        $user->all_books()->attach($book, [
            'characters' => fake()->numberBetween(1, 10),
            'atmosphere' => fake()->numberBetween(1, 10),
            'writing_style' => fake()->numberBetween(1, 10),
            'plot' => fake()->numberBetween(1, 10),
            'intrigue' => fake()->numberBetween(1, 10),
            'logic' => fake()->numberBetween(1, 10),
            'enjoyment' => fake()->numberBetween(1, 10),
        ]);
        $book = $user->all_books->firstWhere('id', $book->id);

        Auth::login($user);

        $component = Livewire::test(Single::class, ['bookId' => $book->id]);

        $this->assertEquals($book->pivot->characters, $component->get('characters'));
        $this->assertEquals($book->pivot->atmosphere, $component->get('atmosphere'));
        $this->assertEquals($book->pivot->writing_style, $component->get('writingStyle'));
        $this->assertEquals($book->pivot->plot, $component->get('plot'));
        $this->assertEquals($book->pivot->intrigue, $component->get('intrigue'));
        $this->assertEquals($book->pivot->logic, $component->get('logic'));
        $this->assertEquals($book->pivot->enjoyment, $component->get('enjoyment'));
    }

    public function test_component_creates_new_book_user_relationship()
    {
        $user = $this->user();
        $book = Book::factory()->create();
        Auth::login($user);

        $this->assertDatabaseCount('book_user', 0);

        Livewire::test(Single::class, ['bookId' => $book->id])
            ->fill([
                'characters' => 1,
                'atmosphere' => 2,
                'writingStyle' => 3,
                'plot' => 4,
                'intrigue' => 5,
                'logic' => 6,
                'enjoyment' => 7,
            ])
            ->call('submit')
            ->assertHasNoErrors();

        $this->assertDatabaseHas('book_user', [
            'book_id' => $book->id,
            'user_id' => $user->id,
            'characters' => 1,
            'atmosphere' => 2,
            'writing_style' => 3,
            'plot' => 4,
            'intrigue' => 5,
            'logic' => 6,
            'enjoyment' => 7,
        ]);
    }

    public function test_component_updates_existing_book_user_relationship()
    {
        $user = $this->user();
        $book = Book::factory()->create();
        $user->all_books()->attach($book);
        Auth::login($user);

        $this->assertDatabaseCount('book_user', 1);

        Livewire::test(Single::class, ['bookId' => $book->id])
            ->fill([
                'characters' => 1,
                'atmosphere' => 2,
                'writingStyle' => 3,
                'plot' => 4,
                'intrigue' => 5,
                'logic' => 6,
                'enjoyment' => 7,
            ])
            ->call('submit')
            ->assertHasNoErrors();

        $this->assertDatabaseCount('book_user', 1);
        $this->assertDatabaseHas('book_user', [
            'book_id' => $book->id,
            'user_id' => $user->id,
            'characters' => 1,
            'atmosphere' => 2,
            'writing_style' => 3,
            'plot' => 4,
            'intrigue' => 5,
            'logic' => 6,
            'enjoyment' => 7,
        ]);
    }
}
