<?php

namespace Tests\Feature\Livewire;

use App\Http\Livewire\Goal;
use Illuminate\Support\Facades\Auth;
use Livewire\Livewire;
use Tests\TestCase;

class GoalTest extends TestCase
{
    public function test_the_component_requires_authorization()
    {
        $user = $this->user();
        Auth::login($user);

        $goal = $this->goal([
            'name' => 'My Goal',
        ]);

        Livewire::test(Goal::class, ['goal' => $goal])
            ->set('goal.name', 'New name')
            ->call('save')
            ->assertForbidden()

            ->call('delete')
            ->assertForbidden();

        $this->assertModelExists($goal);
        $this->assertEquals('My Goal', $goal->fresh()->name);
    }

    public function test_the_component_can_update_data()
    {
        $user = $this->user();
        Auth::login($user);

        $goal = $this->goal([
            'user_id' => $user->id,
            'name' => 'My Goal',
            'start_date' => '2021-01-01',
            'end_date' => '2021-12-31',
            'pages' => 1000,
            'progress_pages' => 10,
        ]);

        Livewire::test(Goal::class, ['goal' => $goal])
            ->assertSeeInOrder([
                'Name',
                'Start Date',
                'End Date',
                'Pages',

                '10 complete out of 1000',

                'Save', 'Delete',
            ])
            ->fill([
                'goal.name' => 'New name',
                'goal.start_date' => '2021-02-01',
                'goal.end_date' => '2021-02-28',
                'goal.pages' => 500,
                'goal.progress_pages' => 100,
            ])
            ->call('save')
            ->assertHasNoErrors();

        $goal->refresh();
        $this->assertEquals('New name', $goal->name);
        $this->assertEquals('2021-02-01', $goal->start_date->format('Y-m-d'));
        $this->assertEquals('2021-02-28', $goal->end_date->format('Y-m-d'));
        $this->assertEquals(100, $goal->progress_pages);
    }

    public function test_the_component_can_delete_a_goal()
    {
        $user = $this->user();
        Auth::login($user);

        $goal = $this->goal([
            'user_id' => $user->id,
        ]);

        Livewire::test(Goal::class, ['goal' => $goal])
            ->call('delete')
            ->assertHasNoErrors()
            ->assertRedirect(route('goals'));

        $this->assertModelMissing($goal);
    }
}
