<?php

namespace Tests\Feature\Models;

use App\Models\Book;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Tests\TestCase;

class BookTest extends TestCase
{
    public static function tearDownAfterClass(): void
    {
        Str::createRandomStringsNormally();

        parent::tearDownAfterClass();
    }

    public function test_it_saves_url_as_cover_image()
    {
        Storage::fake();
        Http::fake([
            'https://example.com/image.jpg' => Http::response('abc'),
        ]);

        Str::createRandomStringsUsing(fn () => 'random');

        $book = Book::factory()->create([
            'cover_image' => null,
        ]);

        $book->saveCoverUrl('https://example.com/image.jpg');

        $this->assertEquals('covers/random.jpg', $book->cover_image);
        Storage::assertExists('public/covers/random.jpg');
    }

    public function test_it_saves_file_as_cover_image()
    {
        Storage::fake();
        $file = UploadedFile::fake()->image('cover.jpg');

        Str::createRandomStringsUsing(fn () => 'random');

        $book = Book::factory()->create([
            'cover_image' => null,
        ]);

        $book->saveCoverFile($file);

        $this->assertEquals('covers/random.jpg', $book->cover_image);
        Storage::assertExists('public/covers/random.jpg');
    }
}
