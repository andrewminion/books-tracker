<?php

namespace Tests;

use App\Models\Goal;
use App\Models\User;
use Illuminate\Foundation\Testing\LazilyRefreshDatabase;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Foundation\Testing\WithFaker;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;
    use LazilyRefreshDatabase;
    use WithFaker;

    protected function goal(array $attributes = []): Goal
    {
        return Goal::factory()->create($attributes);
    }

    protected function user(array $attributes = []): User
    {
        return User::factory()->create($attributes);
    }
}
