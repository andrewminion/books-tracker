<?php

return [
    'filesystem' => env('BOOKS_TRACKER_FILESYSTEM', 'public'),
];
