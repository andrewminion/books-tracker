import 'alpinejs';

/**
 * Toggle pages/length visibility on select change.
 */
[].map.call(document.getElementsByClassName('goal-unit'), function(dropdown) {
    dropdown.addEventListener('change', function(e) {
        var parent = this.closest('.goal');

        if ('time' === this.value) {
            [].map.call(document.querySelectorAll('#'+parent.id+' .goal-type-pages'), function(block) {
                block.classList.add('hidden');
            });
            [].map.call(document.querySelectorAll('#'+parent.id+' .goal-type-time'), function(block) {
                block.classList.remove('hidden');
            });
        } else if ('pages' === this.value) {
            [].map.call(document.querySelectorAll('#'+parent.id+' .goal-type-pages'), function(block) {
                block.classList.remove('hidden');
            });
            [].map.call(document.querySelectorAll('#'+parent.id+' .goal-type-time'), function(block) {
                block.classList.add('hidden');
            });
        }
    });
})

/**
 * Initialize datepicker
 */
flatpickr('.datepicker');
