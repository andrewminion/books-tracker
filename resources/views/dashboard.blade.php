<x-default-layout>
    <x-slot name="header">
        {{ __('Dashboard') }}
    </x-slot>

    <p>Welcome to the books tracker!</p>

    @livewire('series.mine')
</x-default-layout>
