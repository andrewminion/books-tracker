<div>
    <meter class="w-full md:w-3/4 lg:w-1/2 h-12" min="0" max="{{ $length }}" low="{{ $low }}" optimum="{{ $optimum }}" value="{{ $progress }}">{{ $progress }}</meter>
    <p class="text-sm">{{ $progressFormatted }} complete out of {{ $lengthFormatted }}.
    @if ($optimumFormatted)
        Ideally, you should be at {{ $optimumFormatted }}.
    @endif
    </p>
</div>
