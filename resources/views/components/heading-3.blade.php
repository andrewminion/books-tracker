<h3 {{ $attributes->merge(['class' => 'font-semibold text-xl text-gray-800 leading-tight']) }}>
    {{ $slot }}
</h3>
