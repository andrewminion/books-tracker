<h1 {{ $attributes->merge(['class' => 'font-semibold text-3xl text-gray-800 leading-tight']) }}>
    {{ $slot }}
</h1>
