<div>
    @foreach ($goals as $goal)
        @livewire('goal', ['goal' => $goal])
    @endforeach
</div>
