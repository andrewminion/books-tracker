@props([
    'count',
    'total',
])

<p class="text-gray-500">
    {{ __('Completed :count of :total', ['count' => $count, 'total' => $total]) }}
</p>
<meter class="block h-4 w-full md:w-1/2" min="0" max="{{ $total }}" value="{{ $count }}"></meter>
