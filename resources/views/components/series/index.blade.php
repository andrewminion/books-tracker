@php
    $mine = auth()->user()->hasSeries($series);
@endphp

<div class="p-2 flex gap-4 justify-end {{ $mine ? 'bg-green-50' : '' }}">
    <span class="flex-1">
        <x-heading-3 class="inline-block">
            <a href="{{ route('series.show', $series->id) }}">
                {{ $series->name }}
            </a>
        </x-heading-3>
        <div class="text-sm text-gray-500 flex flex-row divide-x">
            @if ($series->people)
                <span class="px-2 first:pl-0">{{ $series->people->pluck('display_name')->implode(', ') }}</span>
            @endif
        </div>

        @if ($mine)
            <div class="text-sm">
                <x-completion-meter :count="$series->user_completed_count" :total="$series->books->count()" />
            </div>
        @endif
    </span>
    <span>
        <a href="{{ route('series.show', $series->id) }}">
            <x-heroicon-o-eye class="text-gray-400 hover:text-gray-500 active:text-green-600 h-6 w-6" />
        </a>
    </span>
    <span class="cursor-pointer group" wire:click="toggleMine({{ $series->id }})">
        <x-heroicon-o-check-circle class="h-6 w-6 {{ $mine ? 'text-green-600 group-hover:text-green-700 group-hover:hidden' : 'text-gray-400 group-hover:text-gray-500 active:text-gray-700' }}" />
        <x-heroicon-o-x-circle class="h-6 w-6 hidden {{ $mine ? 'text-red-600 active:text-red-800 group-hover:block' : '' }}" />
    </span>
</div>
