@props([
    'series',
    'trackSeries' => false,
])

<div class="mt-4">
    <x-heading-2>
        <a href="{{ route('series.show', $series) }}">{{ $series->name }}</a>
    </x-heading-2>

    @if ($series->people)
        <p class="mt-2 text-gray-700">{{ $series->people->pluck('display_name', 'pivot.role')->map(fn ($name, $role) => Str::title($role) . ': ' . $name)->join(', ') }}</p>
    @endif

    <div class="mt-2">
        <x-completion-meter :count="$series->user_completed_count" :total="$series->books->count()" />
    </div>

    <x-button href="{{ route('series.show', $series) }}" class="mt-4">
        {{ __('View Series') }}
        <x-heroicon-s-eye class="inline ml-1 w-5 h-5" />
    </x-button>

    @if ($trackSeries)
        @if (request()->user()->series->contains('id', $series->id))
            <x-button wire:click="toggleMySeries({{ $series->id }})">
                {{ __('Stop Tracking This Series') }}
                <x-heroicon-s-bookmark class="inline w-5 h-5" />
            </x-button>
        @else
            <x-button wire:click="toggleMySeries({{ $series->id }})">
                {{ __('Track This Series') }}
                <x-heroicon-s-bookmark class="inline w-5 h-5" />
            </x-button>
        @endif
    @endif
</div>
