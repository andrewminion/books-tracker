@php
    $completed = $user->hasRead($book);
    $dnf = $user->hasDnf($book);
@endphp

<div class="p-2 flex flex-col sm:flex-row gap-4 {{ $completed ? 'bg-green-50' : '' }} {{ $dnf ? 'bg-yellow-50' : '' }}">
    <span>
        <div {!! filled($book->cover_image) ? 'style="background-image: url(\''.$book->cover_image_url.'\')"' : '' !!} class="flex justify-center align-middle w-24 h-24 bg-contain bg-no-repeat bg-center bg-gray-50 hover:bg-gray-100 group cursor-pointer" onclick="Livewire.emit('openModal', 'modals.book-cover', {{ json_encode(['bookId' => $book->id]) }})">
            <x-heroicon-o-plus-circle class="w-6 h-6 text-gray-400 self-center {{ filled($book->cover_image) ? 'opacity-0 group-hover:opacity-100' : '' }}" />
        </div>
    </span>
    <span>
        <div>
            @if (filled($book->pivot?->display_order))
                <span class="text-gray-500">{{ $book->pivot->display_order }}:</span>
            @endif
            <x-heading-3 class="inline-block">{{ $book->title }}</x-heading-3>
        </div>
        <div class="text-sm text-gray-500 flex flex-row divide-x">
            @if ($book->people->isNotEmpty())
                <span class="px-2 first:pl-0">{{ $book->people->pluck('display_name')->implode(', ') }}</span>
            @endif
            @if ($book->published_at)
                <span class="px-2 first:pl-0">{{ __('Published :date', ['date' => $book->published_at->format('Y')]) }}</span>
            @endif
            @php
                $rating = $book->readers?->firstWhere('id', $user->id)?->pivot?->starRating;
            @endphp
            @if ($rating)
                <span class="rating">
                    @foreach (range(1, $rating) as $star)
                        <x-heroicon-s-star class="inline-block text-yellow-400 h-5 w-5" />
                    @endforeach
                </span>
            @endif
        </div>
    </span>
    <span class="flex-grow"></span>
    <span class="flex flex-row gap-x-2">
        <a href="{{ route('book.show', ['bookId' => $book->id]) }}">
            <x-heroicon-o-annotation class="w-6 h-6 text-gray-400 hover:text-gray-600" />
        </a>
        <x-heroicon-o-minus-circle class="w-6 h-6 {{ $dnf ? 'text-yellow-600 hover:text-yellow-700' : 'text-gray-400 hover:text-yellow-600 active:text-yellow-700' }} cursor-pointer" wire:click="toggleDnf({{ $book->id }})" />
        <x-heroicon-o-check-circle class="w-6 h-6 {{ $completed ? 'text-green-600 hover:text-green-700' : 'text-gray-400 hover:text-green-600 active:text-green-700' }} cursor-pointer" wire:click="toggleComplete({{ $book->id }})" />
    </span>
</div>
