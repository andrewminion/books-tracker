<x-heroicon-s-refresh
    class="ml-2 h-5 w-5 hidden animate-spin"
    {{ $attributes->merge([
        'wire:loading.class.remove' => 'hidden',
        'wire:loading.class' => 'inline-block',
    ]) }}
/>
