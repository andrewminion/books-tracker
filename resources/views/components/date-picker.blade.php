<input
    x-data
    x-ref="input"
    type="text"
    {{ $attributes }}
>
