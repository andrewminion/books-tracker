<x-default-layout>
    <x-slot name="header">
        {{ __('Goals') }}
    </x-slot>

    <x-goals />

    @livewire('new-goal-button')
</x-default-layout>
