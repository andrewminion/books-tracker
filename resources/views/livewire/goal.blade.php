<div id="goal-{{ $goal['id'] }}" class="goal mb-12 flex flex-col md:flex-row">
    <div class="w-1/3 md:w-1/4 mx-auto pr-6">
        @if ($goal->image)
            <img class="cover w-full mb-2" src="{{ $goal->image }}" />
        @else
            <img class="cover w-full mb-2" src="https://via.placeholder.com/150x200.png/cccccc?text=No Photo" />
        @endif

        <form wire:submit.prevent="save">
            <x-jet-input type="file" wire:model.defer="coverImage" />
            @error('coverImage') <x-jet-input-error for="coverImage" class="mt-2" /> @enderror
        </form>
    </div>

    <div class="w-full md:w-3/4">
        <form wire:submit.prevent="save">

            <div class="w-full md:w-3/4 lg:w-1/2 mt-2">
                <x-jet-label for="name" value="{{ __('Name') }}" />
                <x-jet-input type="text" class="mt-1 w-full text-xl" wire:model.defer="goal.name" />
                @error('goal.name') <x-jet-input-error for="goal.name" class="mt-2" /> @enderror
            </div>

            <div class="w-full md:w-3/4 lg:w-1/2 mt-2">
                <x-jet-label for="start_date" value="{{ __('Start Date') }}" />
                <x-date-picker placeholder="{{ now()->format('Y-m-d') }}" class="mt-1 w-full datepicker" wire:model.defer="goal.start_date" />
                @error('goal.start_date') <x-jet-input-error for="goal.start_date" class="mt-2" /> @enderror
            </div>

            <div class="w-full md:w-3/4 lg:w-1/2 mt-2">
                <x-jet-label for="end_date" value="{{ __('End Date') }}" />
                <x-date-picker placeholder="{{ now()->format('Y-m-d') }}" class="mt-1 w-full datepicker" wire:model.defer="goal.end_date" />
                @error('goal.end_date') <x-jet-input-error for="goal.end_date" class="mt-2" /> @enderror
            </div>

            <div class="w-full md:w-3/4 lg:w-1/2 mt-2">
                <x-jet-label for="unit" value="{{ __('Unit') }}" />
                <select class="goal-unit mt-1 w-full" wire:model.defer="goal.unit">
                    @foreach (\App\Enums\UnitEnum::toArray() as $value => $label)
                    <option {{ $goal['unit'] === $value ? 'selected="selected"' : '' }} value="{{ $value }}">{{ ucwords($label) }}</option>
                    @endforeach
                </select>
            </div>

            <div class="w-full md:w-3/4 lg:w-1/2 mt-2 goal-type-pages {{ $goal['unit'] !== 'pages' ? 'hidden' : '' }}">
                <x-jet-label for="pages" value="{{ __('Pages') }}" />
                <x-jet-input type="text" class="mt-1 w-full" wire:model.defer="goal.pages" />
                @error('goal.pages') <x-jet-input-error for="goal.pages" class="mt-2" /> @enderror
            </div>

            <div class="w-full md:w-3/4 lg:w-1/2 mt-2 goal-type-pages {{ $goal['unit'] !== 'pages' ? 'hidden' : '' }}">
                <x-jet-label for="progress_pages" value="{{ __('Current Progress') }}" />
                <x-jet-input type="text" class="mt-1 w-full" wire:model.defer="goal.progress_pages" />
                @error('goal.progress_pages') <x-jet-input-error for="goal.progress_pages" class="mt-2" /> @enderror
            </div>

            <div class="w-full md:w-3/4 lg:w-1/2 mt-2 goal-type-time {{ $goal['unit'] !== 'time' ? 'hidden' : '' }}">
                <x-jet-label for="time" value="{{ __('Length') }}" />
                <x-jet-input type="text" class="mt-1 w-full" wire:model.defer="goal.time" />
                @error('goal.time') <x-jet-input-error for="goal.time" class="mt-2" /> @enderror
            </div>

            <div class="w-full md:w-3/4 lg:w-1/2 mt-2 goal-type-time {{ $goal['unit'] !== 'time' ? 'hidden' : '' }}">
                <x-jet-label for="progress_time" value="{{ __('Current Progress') }}" />
                <x-jet-input type="text" class="mt-1 w-full" wire:model.defer="goal.progress_time" />
                @error('goal.progress_time') <x-jet-input-error for="goal.progress_time" class="mt-2" /> @enderror
            </div>


            @if (($goal->pages && $goal->progress_pages) || ($goal->time && $goal->progress_time))
                <x-goal-progress :goal="$goal" />
            @endif

            <x-button wire:loading.attr="disabled">Save</x-button>
            <x-jet-danger-button class="ml-2" wire:click="delete" wire:loading.attr="disabled">Delete</x-jet-danger-button>

        </form>
    </div>
</div>
