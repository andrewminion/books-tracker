<div>
    <x-slot name="header">
        {{ $book->title }}
    </x-slot>

    @if ($book->cover_image)
        <img class="max-h-64" src="{{ $book->cover_image_url }}" alt="{{ $book->title }}" />
    @endif

    @if ($book->description)
        <x-heading-2 class="mt-4">{{ __('Description') }}</x-heading-2>
        <p class="mt-4">{{ $book->description }}</p>
    @endif

    @auth
        <x-heading-2 class="mt-4">{{ __('Review') }}</x-heading-2>

        <form wire:submit.prevent="submit">
            <div class="mt-4 grid grid-cols-2 gap-4">
                <label for="characters">{{ __('Characters') }}</label>
                <input name="characters" wire:model.lazy="characters" class="w-24" type="number" min="1" max="10" />

                <label for="atmosphere">{{ __('Atmosphere/Setting') }}</label>
                <input name="atmosphere" wire:model.lazy="atmosphere" class="w-24" type="number" min="1" max="10" />

                <label for="writingStyle">{{ __('Writing Style') }}</label>
                <input name="writingStyle" wire:model.lazy="writingStyle" class="w-24" type="number" min="1" max="10" />

                <label for="plot">{{ __('Plot') }}</label>
                <input name="plot" wire:model.lazy="plot" class="w-24" type="number" min="1" max="10" />

                <label for="intrigue">{{ __('Intrigue') }}</label>
                <input name="intrigue" wire:model.lazy="intrigue" class="w-24" type="number" min="1" max="10" />

                <label for="logic">{{ __('Logic/Relationships') }}</label>
                <input name="logic" wire:model.lazy="logic" class="w-24" type="number" min="1" max="10" />

                <label for="enjoyment">{{ __('Enjoyment') }}</label>
                <input name="enjoyment" wire:model.lazy="enjoyment" class="w-24" type="number" min="1" max="10" />

                <p class="mt-4">
                    Total: {{ $this->decimalRating }}
                </p>

                @if ($this->starRating)
                    <p class="mt-4">
                        @foreach (range(1, $this->starRating) as $star)
                            <x-heroicon-s-star class="inline-block text-yellow-400 h-5 w-5" />
                        @endforeach
                    </p>
                @endif
            </div>

            <x-button class="mt-4" type="submit">{{ __('Save Rating') }}</x-button>
        </form>
    @endauth
</div>
