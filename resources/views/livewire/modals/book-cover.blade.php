<div class="p-6">
    <form wire:submit.prevent="submit">
        <p class="mt-4 text-gray-700">
            {{ __('Either upload an image or paste a URL') }}
        </p>

        <label for="image" class="block mt-4 text-gray-700">{{ __('Upload a cover') }}</label>
        <input
            name="image"
            id="image"
            type="file"
            wire:model="image"
            />

        @error('image') <div class="error font-bold text-red-700">{{ $message }}</div> @enderror

        <label for="url" class="block mt-4 text-gray-700">{{ __('Paste a URL') }}</label>
        <input
            type="url"
            name="url"
            id="url"
            wire:model.defer="url"
            class="block w-full"
            />

        <div class="mt-4">
            <x-button href="#" wire:click="closeModal">Cancel</x-button>
            <x-button type="submit" class="block mt-4">Save Cover</x-button>
        </div>
    </form>
</div>
