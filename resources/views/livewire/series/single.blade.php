<div>
    <x-slot name="header">
        {{ $name }}
    </x-slot>

    @if ($cover_image)
        <img class="max-h-64" src="{{ $series->cover_image_url }}" alt="{{ $name }}" />
    @endif

    @if ($description)
        <x-heading-2 class="mt-4">{{ __('Description') }}</x-heading-2>
        <p class="mt-4">{{ $description }}</p>
    @endif

    <x-heading-2 class="mt-4 mb-4">{{ __('Books') }}</x-heading-2>

    @php
        $user = auth()->user()->load(['all_books', 'series']);
    @endphp

    @if ($user->hasSeries($series))
        <div class="text-sm mb-4">
            <x-completion-meter :count="$series->user_completed_count" :total="$this->books->count()" />
        </div>
    @endif

    @foreach ($books as $book)
        <x-series.book :book="$book" :user="$user" />
    @endforeach
</div>
