<div>
    <x-slot name="header">
        {{ __('My Series') }}
    </x-slot>

    <div class="mt-4 mb-4">
        <x-button href="{{ route('series.index') }}">
            {{ __('All Series') }}
        </x-button>
    </div>

    @foreach ($mySeries as $series)
        <x-series.index :series="$series" />
    @endforeach
</div>
