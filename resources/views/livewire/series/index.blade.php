<div>
    <x-slot name="header">
        {{ __('All Series') }}
    </x-slot>

    <input class="my-4" type="search" wire:model="search" name="search" placeholder="{{ __('Search for a series') }}" />

    <form wire:submit.prevent="addSeries">
        <input class="my-4" type="url" wire:model.lazy="newSeries" name="newSeries" placeholder="{{ __('Add a series') }}" />
        <x-button type="submit">
            {{ __('Add Series') }}
            <x-spinner wire:target="addSeries" />
        </x-button>
    </form>

    @foreach ($allSeries as $series)
        <x-series.index :series="$series" />
    @endforeach
</div>
