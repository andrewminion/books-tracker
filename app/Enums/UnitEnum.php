<?php

namespace App\Enums;

use Spatie\Enum\Laravel\Enum;

/**
 * @method static self pages()
 * @method static self time()
 */
final class UnitEnum extends Enum
{
}
