<?php

namespace App\Enums;

use Spatie\Enum\Laravel\Enum;

/**
 * @method static self read()
 * @method static self dnf()
 */
final class CompletionStatusEnum extends Enum
{
    protected static function labels()
    {
        return [
            'dnf' => 'Did Not Finish',
            'read' => 'Read',
        ];
    }
}
