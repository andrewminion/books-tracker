<?php

namespace App\View\Components;

use Illuminate\Support\Facades\Auth;
use Illuminate\View\Component;

class Goals extends Component
{
    /** @var \App\Models\User */
    public $user;

    /** @var \Illuminate\Database\Eloquent\Collection|\App\Models\Goal[] */
    public $goals;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->user = Auth::user();
        $this->goals = $this->user->goals;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.goals');
    }
}
