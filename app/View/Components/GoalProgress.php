<?php

namespace App\View\Components;

use App\Helpers\Time;
use App\Models\Goal;
use Illuminate\View\Component;

class GoalProgress extends Component
{
    /** @var float */
    public $progress;

    /** @var float|string */
    public $progressFormatted;

    /** @var float */
    public $length;

    /** @var float|string */
    public $lengthFormatted;

    /** @var float */
    public $low;

    /** @var float */
    public $optimum;

    /** @var float|string */
    public $optimumFormatted;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(Goal $goal)
    {
        if ('time' === $goal->unit) {
            $this->length = Time::timeToMinutes($goal->time);
            $this->lengthFormatted = $goal->time;
            $this->progress = Time::timeToMinutes($goal->progress_time);
            $this->progressFormatted = $goal->progress_time;
        } else {
            $this->length = $goal->length;
            $this->lengthFormatted = $goal->length;
            $this->progress = $goal->progress_pages;
            $this->progressFormatted = $goal->progress_pages;
        }

        if ($goal->start_date && $goal->end_date) {
            $totalDays = $goal->end_date->diffInDays($goal->start_date);
            $elapsedDays = now()->diffInDays($goal->start_date);

            if ('time' === $goal->unit) {
                $minutes = Time::timeToMinutes($goal->time);
                $this->optimum = min((($minutes / $totalDays) * $elapsedDays), $minutes);
                $this->optimumFormatted = Time::minutesToTime($this->optimum);
            } else {
                $this->optimum = round(min((($goal->pages / $totalDays) * $elapsedDays), $goal->pages));
                $this->optimumFormatted = $this->optimum;
            }
            $this->low = $this->optimum * 0.8;
        }
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.goal-progress');
    }
}
