<?php

namespace App\Http\Livewire\Book;

use App\Models\Book;
use Livewire\Component;

/**
 * @property-read ?float $decimalRating
 * @property-read ?int $starRating
 */
class Single extends Component
{
    public int $bookId;

    public Book $book;

    public int $characters = 0;

    public int $atmosphere = 0;

    public int $writingStyle = 0;

    public int $plot = 0;

    public int $intrigue = 0;

    public int $logic = 0;

    public int $enjoyment = 0;

    protected $rules = [
        'characters' => ['required', 'numeric', 'min:0', 'max:10'],
        'atmosphere' => ['required', 'numeric', 'min:0', 'max:10'],
        'writingStyle' => ['required', 'numeric', 'min:0', 'max:10'],
        'plot' => ['required', 'numeric', 'min:0', 'max:10'],
        'intrigue' => ['required', 'numeric', 'min:0', 'max:10'],
        'logic' => ['required', 'numeric', 'min:0', 'max:10'],
        'enjoyment' => ['required', 'numeric', 'min:0', 'max:10'],
    ];

    public function mount(int $bookId)
    {
        /** @var \App\Models\User $user */
        $user = auth()->user();

        $this->book = $user?->all_books()->firstWhere('book_id', $bookId) ?? Book::find($bookId); // @phpstan-ignore-line

        if (isset($this->book->pivot)) {
            $this->fill([
                'characters' => $this->book->pivot['characters'] ?? 0,
                'atmosphere' => $this->book->pivot['atmosphere'] ?? 0,
                'writingStyle' => $this->book->pivot['writing_style'] ?? 0,
                'plot' => $this->book->pivot['plot'] ?? 0,
                'intrigue' => $this->book->pivot['intrigue'] ?? 0,
                'logic' => $this->book->pivot['logic'] ?? 0,
                'enjoyment' => $this->book->pivot['enjoyment'] ?? 0,
            ]);
        }
    }

    public function getDecimalRatingProperty(): float
    {
        $total = collect([
            $this->characters,
            $this->atmosphere,
            $this->writingStyle,
            $this->plot,
            $this->intrigue,
            $this->logic,
            $this->enjoyment,
        ])->sum();

        return round($total / 7, 2);
    }

    public function getStarRatingProperty(): int
    {
        return match (true) {
            $this->decimalRating > 9 => 5,
            $this->decimalRating > 7 => 4,
            $this->decimalRating > 4.5 => 3,
            $this->decimalRating > 2.2 => 2,
            $this->decimalRating > 1 => 1,
            default => 0,
        };
    }

    public function render()
    {
        return view('livewire.book.single');
    }

    public function submit()
    {
        $this->validate();

        $rating = [
            'characters' => $this->characters,
            'atmosphere' => $this->atmosphere,
            'writing_style' => $this->writingStyle,
            'plot' => $this->plot,
            'intrigue' => $this->intrigue,
            'logic' => $this->logic,
            'enjoyment' => $this->enjoyment,
        ];

        /** @var \App\Models\User $user */
        $user = auth()->user();

        if ($user->all_books()->wherePivot('book_id', $this->bookId)->count() > 0) {
            $user->all_books()->updateExistingPivot($this->bookId, $rating);
        } else {
            $user->all_books()->attach($this->bookId, $rating);
        }
    }
}
