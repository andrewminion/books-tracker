<?php

namespace App\Http\Livewire\Modals;

use App\Models\Book;
use Livewire\WithFileUploads;
use LivewireUI\Modal\ModalComponent;

class BookCover extends ModalComponent
{
    use WithFileUploads;

    public int $bookId;

    /** @var string|\Livewire\TemporaryUploadedFile */
    public $image = null;

    public ?string $url = null;

    public function render()
    {
        return view('livewire.modals.book-cover');
    }

    public function submit()
    {
        $this->validate([
            'image' => [
                'required_without:url',
                'nullable',
                'image',
                'max:2048',
            ],
            'url' => [
                'required_without:image',
                'nullable',
                'string',
                'url',
            ],
        ]);

        /** @var Book $book */
        $book = Book::find($this->bookId);

        if (isset($this->url)) {
            $book->saveCoverUrl($this->url);
        } else {
            $book->saveCoverFile($this->image);
        }

        $this->closeModalWithEvents([
            'coverUploaded',
        ]);
    }
}
