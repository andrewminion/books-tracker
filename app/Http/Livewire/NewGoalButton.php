<?php

namespace App\Http\Livewire;

use App\Models\Goal;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class NewGoalButton extends Component
{
    public function render()
    {
        return view('livewire.new-goal-button');
    }

    public function createGoal()
    {
        (new Goal([
            'name' => 'New Goal',
            'user_id' => Auth::user()->id,
        ]))->save();

        return redirect()->route('goals');
    }
}
