<?php

namespace App\Http\Livewire;

use App\Models\Goal as GoalModel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithFileUploads;

class Goal extends Component
{
    use WithFileUploads;

    public GoalModel $goal;

    /** @var \Livewire\TemporaryUploadedFile */
    public $coverImage;

    protected $rules = [
        'coverImage' => 'nullable|image',

        'goal.name' => 'required|string|min:3',
        'goal.start_date' => 'date',
        'goal.end_date' => 'date',
        'goal.pages' => 'int',
        'goal.progress_pages' => 'int',
        'goal.time' => 'nullable|string',
        'goal.progress_time' => 'nullable|string',
        'goal.unit' => 'string|in:pages,time',
    ];

    public function render()
    {
        return view('livewire.goal');
    }

    public function save()
    {
        abort_unless($this->goal->user->is(Auth::user()), 403);

        $this->validate();
        if ($this->coverImage) {
            $this->coverImage->storePubliclyAs('covers', $this->coverImage->getClientOriginalName(), config('books-tracker.filesystem'));
            $this->goal->image = Storage::disk(config('books-tracker.filesystem'))->url('covers/'.$this->coverImage->getClientOriginalName());
        }

        $this->goal->save();
    }

    public function delete()
    {
        abort_unless($this->goal->user->is(Auth::user()), 403);

        $this->goal->delete();

        return redirect()->route('goals');
    }
}
