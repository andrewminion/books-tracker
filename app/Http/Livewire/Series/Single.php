<?php

namespace App\Http\Livewire\Series;

use App\Models\Series;
use Illuminate\Database\Eloquent\Collection;
use Livewire\Component;

class Single extends Component
{
    public Series $series;

    public int $modelId;

    public string $name;

    public ?string $cover_image;

    public ?string $description;

    public ?Collection $books;

    protected $listeners = [
        'coverUpdated' => 'render',
    ];

    public function mount(Series $id): void
    {
        $this->modelId = $id->id;
        $this->fill($id);
        $this->series = $id;
        $this->loadRelationships($id);
    }

    public function hydrate()
    {
        $this->loadRelationships(Series::find($this->modelId));
    }

    public function render()
    {
        return view('livewire.series.single');
    }

    public function toggleComplete($bookId): void
    {
        /** @var \App\Models\User */
        $user = auth()->user();
        $user->completed_books()->toggle($bookId);
        if ($user->dnf_books->contains($bookId)) {
            $user->dnf_books()->detach($bookId);
        }
        $user->load('dnf_books');
    }

    public function toggleDnf($bookId): void
    {
        /** @var \App\Models\User */
        $user = auth()->user();
        $user->dnf_books()->toggle($bookId);
        if ($user->completed_books->contains($bookId)) {
            $user->completed_books()->detach($bookId);
        }
        $user->load('completed_books');
    }

    private function loadRelationships(Series $series): void
    {
        $this->books = $series
            ->books()
            ->withPivot(['display_order', 'sort_order'])
            ->with([
                'people',
                'readers',
            ])
            ->get();
    }
}
