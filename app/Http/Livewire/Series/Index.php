<?php

namespace App\Http\Livewire\Series;

use App\Jobs\Series\Import;
use App\Models\Series;
use Illuminate\Database\Eloquent\Collection;
use Livewire\Component;

/**
 * @property Collection<Series> $all_series
 */
class Index extends Component
{
    public string $search = '';

    public string $newSeries = '';

    protected $rules = [
        'search' => 'nullable|string|max:255',
        'newSeries' => 'nullable|string|max:255',
    ];

    public function render()
    {
        return view('livewire.series.index', [
            'allSeries' => $this->all_series,
        ]);
    }

    public function getAllSeriesProperty(): Collection
    {
        return Series::with('people')
            ->orderBy('name')
            ->get();
    }

    public function updatedSearch($search)
    {
        $this->all_series = Series::where('name', 'like', '%'.$search.'%')
            ->with('people')
            ->orderBy('name')
            ->get();
    }

    public function toggleMine($seriesId): void
    {
        /** @var \App\Models\User */
        $user = auth()->user();
        $user->series()->toggle($seriesId);
    }

    public function addSeries()
    {
        (new Import($this->newSeries))->handle();
        $this->newSeries = '';
    }
}
