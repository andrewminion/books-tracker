<?php

namespace App\Http\Livewire\Series;

use App\Models\Series;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

/**
 * @property Collection<Series> $my_series
 */
class Mine extends Component
{
    public string $search = '';

    public string $newSeries = '';

    protected $rules = [
        'search' => 'nullable|string|max:255',
        'newSeries' => 'nullable|string|max:255',
    ];

    public function render()
    {
        return view('livewire.series.mine', [
            'mySeries' => $this->my_series,
        ]);
    }

    public function getMySeriesProperty(): Collection
    {
        return Auth::user()
            ->series()
            ->orderBy('name')
            ->get();
    }

    public function updatedSearch($search)
    {
        $this->my_series = Series::where('name', 'like', '%'.$search.'%')
            ->with('people')
            ->orderBy('name')
            ->get();
    }

    public function toggleMine($seriesId): void
    {
        /** @var \App\Models\User */
        $user = auth()->user();
        $user->series()->toggle($seriesId);
    }
}
