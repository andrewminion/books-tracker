<?php

namespace App\Models\Traits;

use Illuminate\Support\Facades\Storage;

/**
 * @property-read string $cover_image_url
 */
trait HasCoverImage
{
    public function getCoverImageUrlAttribute(): string
    {
        return Storage::url($this->cover_image);
    }
}
