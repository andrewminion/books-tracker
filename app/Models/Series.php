<?php

namespace App\Models;

use App\Models\Traits\HasCoverImage;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * App\Models\Series
 *
 * @property int $id
 * @property string $name
 * @property string|null $description
 * @property string|null $cover_image
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection<\App\Models\Book> $books
 * @property-read int|null $books_count
 * @property-read int $user_completed_count
 * @property-read float $user_completed_percentage
 * @property-read \Illuminate\Database\Eloquent\Collection<\App\Models\Person> $people
 * @property-read int|null $people_count
 * @property-read \Illuminate\Database\Eloquent\Collection<\App\Models\User> $users
 * @property-read int|null $users_count
 *
 * @method static \Database\Factories\SeriesFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|Series newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Series newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Series query()
 * @method static \Illuminate\Database\Eloquent\Builder|Series whereCoverImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Series whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Series whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Series whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Series whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Series whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Series extends Model
{
    use HasFactory;
    use HasCoverImage;

    protected $fillable = [
        'name',
        'description',
        'cover_image',
    ];

    public function books(): BelongsToMany
    {
        return $this->belongsToMany(Book::class)->withPivot([
            'sort_order',
            'display_order',
        ]);
    }

    public function people(): BelongsToMany
    {
        return $this->belongsToMany(Person::class)->withPivot(['role']);
    }

    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class)->withPivot(['role']);
    }

    public function getUserCompletedCountAttribute(): int
    {
        return $this->books->intersect(auth()->user()->all_books)->count();
    }

    public function getUserCompletedPercentageAttribute(): float
    {
        $total = $this->books->count();
        $completedCount = $this->books->intersect(auth()->user()->all_books)->count();

        return $total / $completedCount;
    }
}
