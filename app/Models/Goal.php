<?php

namespace App\Models;

use App\Helpers\Time;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\Goal
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $user_id
 * @property string $name
 * @property string|null $image
 * @property \Illuminate\Support\Carbon|null $start_date
 * @property \Illuminate\Support\Carbon|null $end_date
 * @property string|null $unit
 * @property int|null $pages
 * @property int|null $progress_pages
 * @property string|null $time
 * @property string|null $progress_time
 * @property float|null $percentage
 * @property-read int|string $length
 * @property-write mixed $progress
 * @property-read \App\Models\User $user
 *
 * @method static \Database\Factories\GoalFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|Goal newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Goal newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Goal query()
 * @method static \Illuminate\Database\Eloquent\Builder|Goal whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Goal whereEndDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Goal whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Goal whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Goal whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Goal wherePages($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Goal wherePercentage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Goal whereProgressPages($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Goal whereProgressTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Goal whereStartDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Goal whereTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Goal whereUnit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Goal whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Goal whereUserId($value)
 * @mixin \Eloquent
 */
class Goal extends Model
{
    use HasFactory;

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'start_date' => 'date:Y-m-d',
        'end_date' => 'date:Y-m-d',
        // Don’t cast to UnitEnum or Livewire won’t work.
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array<int, string>
     */
    protected $appends = [
        'length',
    ];

    /**
     * Fillable attributes.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'start_date',
        'end_date',
        'pages',
        'progress_pages',
        'progress_time',
        'time',
        'unit',
        'user_id',
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * If unit is time, format length as time.
     *
     * @param  int  $value
     * @return int|string
     *
     * @since 1.0.0
     */
    public function getLengthAttribute($value)
    {
        if ('time' === $this->unit) {
            return $this->time;
        }

        return $this->pages;
    }

    /**
     * Set the percentage.
     *
     * @param  int  $value
     * @return void
     *
     * @since 1.0.0
     */
    public function setProgressPagesAttribute($value)
    {
        $this->attributes['progress_pages'] = $value;
        if ('pages' === $this->unit) {
            $this->setProgressAttribute($value);
        }
    }

    /**
     * Set the percentage.
     *
     * @param  string  $value
     * @return void
     *
     * @since 1.0.0
     */
    public function setProgressTimeAttribute($value)
    {
        $this->attributes['progress_time'] = $value;
        if ('time' === $this->unit) {
            $this->setProgressAttribute($value);
        }
    }

    /**
     * Set the percentage.
     *
     * @param  mixed  $value
     * @return void
     *
     * @since 1.0.0
     */
    public function setProgressAttribute($value)
    {
        if (empty($value) || empty($this->length)) {
            return;
        }

        if ('time' === $this->unit) {
            $this->attributes['percentage'] = Time::timeToMinutes($value) / Time::timeToMinutes($this->time);
        } else {
            $this->attributes['percentage'] = $value / $this->length;
        }
    }
}
