<?php

namespace App\Models;

use App\Enums\CompletionStatusEnum;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\Pivot;

/**
 * App\Models\UserBook
 *
 * @property int $id
 * @property int $book_id
 * @property int $user_id
 * @property CompletionStatusEnum $status
 * @property \Illuminate\Support\Carbon|null $completed_at
 * @property int $characters
 * @property int $atmosphere
 * @property int $writing_style
 * @property int $plot
 * @property int $intrigue
 * @property int $logic
 * @property int $enjoyment
 * @property int $notes
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Book $book
 * @property-read \App\Models\User $user
 * @property-read float $decimal_rating
 * @property-read int $star_rating
 *
 * @method static \Database\Factories\UserBookFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|UserBook newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserBook newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserBook query()
 * @method static \Illuminate\Database\Eloquent\Builder|UserBook whereBookId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserBook whereCompletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserBook whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserBook whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserBook whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserBook whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserBook whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserBook whereCharacters($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserBook whereAtmosphere($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserBook whereWritingStyle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserBook wherePlot($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserBook whereIntrigue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserBook whereLogic($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserBook whereEnjoyment($value)
 * @mixin \Eloquent
 */
class UserBook extends Pivot
{
    use HasFactory;

    protected $casts = [
        'completed_at' => 'date',
        'status' => CompletionStatusEnum::class,
    ];

    public function book(): BelongsTo
    {
        return $this->belongsTo(Book::class);
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function getDecimalRatingAttribute(): float
    {
        $total = collect([
            $this->characters,
            $this->atmosphere,
            $this->writing_style,
            $this->plot,
            $this->intrigue,
            $this->logic,
            $this->enjoyment,
        ])->sum();

        return round($total / 7, 2);
    }

    public function getStarRatingAttribute(): int
    {
        return match (true) {
            $this->decimalRating > 9 => 5,
            $this->decimalRating > 7 => 4,
            $this->decimalRating > 4.5 => 3,
            $this->decimalRating > 2.2 => 2,
            $this->decimalRating > 1 => 1,
            default => 0,
        };
    }
}
