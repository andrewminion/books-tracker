<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * App\Models\BookList
 *
 * @property int $id
 * @property int $user_id
 * @property string $name
 * @property string|null $description
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection<\App\Models\Book> $books
 * @property-read int|null $books_count
 * @property-read \App\Models\User $user
 *
 * @method static \Database\Factories\BookListFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|BookList newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BookList newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BookList query()
 * @method static \Illuminate\Database\Eloquent\Builder|BookList whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BookList whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BookList whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BookList whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BookList whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BookList whereUserId($value)
 * @mixin \Eloquent
 */
class BookList extends Model
{
    use HasFactory;

    public function books(): BelongsToMany
    {
        return $this->belongsToMany(Book::class)->withPivot(['role']);
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
