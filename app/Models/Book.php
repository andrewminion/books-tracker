<?php

namespace App\Models;

use App\Models\Traits\HasCoverImage;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Support\Stringable;
use League\Flysystem\Visibility;
use RuntimeException;

/**
 * App\Models\Book
 *
 * @property int $id
 * @property string $title
 * @property string|null $cover_image
 * @property \Illuminate\Support\Carbon|null $published_at
 * @property string|null $description
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection<\App\Models\Person> $people
 * @property-read int|null $people_count
 * @property-read \Illuminate\Database\Eloquent\Collection<\App\Models\User> $readers
 * @property-read int|null $readers_count
 * @property-read \Illuminate\Database\Eloquent\Collection<\App\Models\Series> $series
 * @property-read int|null $series_count
 *
 * @method static \Database\Factories\BookFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|Book newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Book newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Book query()
 * @method static \Illuminate\Database\Eloquent\Builder|Book whereCoverImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Book whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Book whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Book whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Book wherePublishedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Book whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Book whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Book extends Model
{
    use HasFactory;
    use HasCoverImage;

    protected $casts = [
        'published_at' => 'date',
    ];

    protected $fillable = [
        'title',
        'cover_image',
        'description',
        'published_at',
    ];

    protected $appends = [
        'cover_image_url',
    ];

    public function people(): BelongsToMany
    {
        return $this->belongsToMany(Person::class)->withPivot(['role']);
    }

    public function series(): BelongsToMany
    {
        return $this->belongsToMany(Series::class)->withPivot([
            'sort_order',
            'display_order',
        ]);
    }

    public function readers(): BelongsToMany
    {
        return $this
            ->belongsToMany(User::class)
            ->using(UserBook::class)
            ->withPivot([
                'status',
                'characters',
                'atmosphere',
                'writing_style',
                'plot',
                'intrigue',
                'logic',
                'enjoyment',
                'notes',
            ])
            ->withTimestamps();
    }

    public function saveCoverFile(UploadedFile $file, bool $saveModel = true): self
    {
        $stored = $file->storePublicly('public/covers');

        return $this->finishSavingCover($stored, $saveModel);
    }

    public function saveCoverUrl(string $url, bool $saveModel = true): self
    {
        $image = Http::get($url);
        $image->throw();

        $hashName = Str::random(40);
        $extension = str($url)
            ->basename()
            ->whenContains(
                needles: '.',
                callback: fn (Stringable $url) => $url->afterLast('.')->prepend('.'),
                default: fn () => '',
            );

        $path = 'public/covers/'.$hashName.$extension;
        $stored = Storage::put($path, $image->body(), Visibility::PUBLIC);

        return $this->finishSavingCover($stored ? $path : $stored, $saveModel);
    }

    private function finishSavingCover(string|bool $path, bool $saveModel): self
    {
        if (! $path) {
            throw new RuntimeException('Unable to save image');
        }

        $this->cover_image = str($path)->after('public/');

        if ($saveModel) {
            $this->save();
        }

        return $this;
    }
}
