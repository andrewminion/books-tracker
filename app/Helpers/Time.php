<?php

namespace App\Helpers;

class Time
{
    /**
     * Convert time string to number of nimutes.
     *
     * @param  string  $time
     * @return int
     *
     * @since 1.0.0
     */
    public static function timeToMinutes($time)
    {
        $time = explode(':', $time);

        return ($time[0] * 60) + $time[1];
    }

    /**
     * Convert time string to number of nimutes.
     *
     * @param  int  $minutes
     * @return string
     *
     * @since 1.0.0
     */
    public static function minutesToTime($minutes)
    {
        return floor($minutes / 60).':'.($minutes % 60).':00';
    }
}
