<?php

namespace App\Imports\Books;

use App\Exceptions\SeriesImportFailedException;
use App\Models\Book;
use App\Models\Series;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Http;

class TheStoryGraph implements ShouldQueue
{
    private Series $series;

    public function __construct(
        private string $url,
    ) {
        //
    }

    public function handle()
    {
        if (! str($this->url)->contains('app.thestorygraph.com/series/')) {
            throw new SeriesImportFailedException('Invalid URL', 400);
        }

        $series = Http::get($this->url);

        if (! $series->ok()) {
            $e = $series->toException();
            throw new SeriesImportFailedException('Failed to import '.$this->url.' with this error: '.$e->getMessage(), $e->getCode(), $e);
        }

        $response = str($series->body());

        $seriesName = $response->betweenFirst('page-heading">', '<'); //?

        $this->series = Series::firstOrCreate([
            'name' => $seriesName,
        ]);

        $response
            ->between('series-books-panes">', '<div id="infinite-scroll">')
            ->explode('book-pane-content')
            ->skip(1)
            ->map(function (string $book) {
                $book = str($book);

                return [
                    'title' => $book
                        ->after('book-title-author-and-series')
                        ->after('<a href="/books/')
                        ->after('">')
                        ->betweenFirst('">"', '</a>'),
                    'order' => $book
                        ->after('book-title-author-and-series')
                        ->after('</h3>')
                        ->after('</a>')
                        ->betweenFirst('">#', '</a>'),
                    'published' => $book
                        ->after('book-title-author-and-series')
                        ->betweenFirst('first published ', '</p>'),
                    'cover' => $book
                        ->after('book-cover')
                        ->after('<img')
                        ->betweenFirst('src="', '"'),
                ];
            })
            ->each(function (array $bookData, $index) {
                $book = new Book([
                    'title' => $bookData['title'],
                    'published_at' => new Carbon($bookData['published'].'-01-01'),
                ]);

                if (filled($bookData['cover']) && ! str($bookData['cover'])->contains('placeholder-cover')) {
                    $book->saveCoverUrl($bookData['cover'], false);
                }

                $book->save();

                $this->series->books()->attach($book, [
                    'display_order' => $bookData['order'],
                    'sort_order' => $index,
                ]);
            });
    }
}
