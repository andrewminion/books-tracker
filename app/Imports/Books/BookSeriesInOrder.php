<?php

namespace App\Imports\Books;

use App\Exceptions\SeriesImportFailedException;
use App\Models\Book;
use App\Models\Series;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\OnEachRow;
use Maatwebsite\Excel\Row;

class BookSeriesInOrder implements OnEachRow
{
    use Importable;

    private Series $series;

    private int $seriesOrder = 1;

    private ?string $filePath; // @phpstan-ignore-line

    public function __construct(
        private string $url,
    ) {
        if (! str($this->url)->contains('bookseriesinorder.com/')) {
            throw new SeriesImportFailedException('Invalid URL', 400);
        }

        $series = Http::get($this->url);

        if (! $series->ok()) {
            $e = $series->toException();
            throw new SeriesImportFailedException('Failed to import '.$this->url.' with this error: '.$e->getMessage(), $e->getCode(), $e);
        }

        $response = str($series->body());

        $seriesName = $response
            ->between('<title>', '</title>')
            ->before(' - Book Series In Order')
            ->pipe('htmlspecialchars_decode');

        $this->series = Series::firstOrCreate([
            'name' => $seriesName,
        ]);

        $table = $response
            ->betweenFirst("<div class='list'>", '</table>')
            ->replace('&', '&amp;')
            ->append('</table>');

        $localPath = 'tmp/'.hash('sha256', $this->url).'.html';
        Storage::disk('local')->put($localPath, $table);

        $this->filePath = Storage::disk('local')->path($localPath);
    }

    public function onRow(Row $row)
    {
        $book = new Book([
            'title' => $row[0],
            'published_at' => new Carbon(str($row[1])->between('(', ')').'-01-01'),
        ]);
        $book->save();

        $this->series->books()->attach($book, [
            'display_order' => $this->seriesOrder,
            'sort_order' => $this->seriesOrder,
        ]);
        $this->seriesOrder++;
    }
}
