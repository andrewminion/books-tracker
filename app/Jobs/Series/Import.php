<?php

namespace App\Jobs\Series;

use App\Exceptions\SeriesImportFailedException;
use App\Imports\Books\BookSeriesInOrder;
use App\Imports\Books\TheStoryGraph;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Maatwebsite\Excel\Excel;

class Import implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /** @abstract \Maatwebsite\Excel\Concerns\Importable|ShouldQueue $import */
    private $import;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(
        public string $newSeries,
    ) {
        $seriesUrl = str($this->newSeries);

        $this->import = match (true) {
            $seriesUrl->contains('bookseriesinorder.com') => new BookSeriesInOrder($this->newSeries),
            $seriesUrl->contains('thestorygraph.com') => new TheStoryGraph($this->newSeries),
            default => null,
        };

        if (is_null($this->import)) {
            throw new SeriesImportFailedException('Unsupported URL', 400);
        }
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (method_exists($this->import, 'import')) {
            $this->import->import(readerType: Excel::HTML);
        } else {
            $this->import->handle();
        }
    }
}
