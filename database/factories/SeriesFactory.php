<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @method \App\Models\Series|\Illuminate\Support\Collection<\App\Models\Series> create($attributes = [], ?Model $parent = null)
 * @method \Illuminate\Support\Collection<\App\Models\Series> createMany(iterable $records)
 * @method \App\Models\Series createOne($attributes = [])
 * @method \App\Models\Series|\Illuminate\Support\Collection<\App\Models\Series> make($attributes = [], ?Model $parent = null)
 * @method \App\Models\Series makeOne($attributes = [])
 */
class SeriesFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'name' => $this->faker->words(3, true),
            'description' => $this->faker->sentence(),
        ];
    }
}
