<?php

namespace Database\Factories;

use App\Enums\UnitEnum;
use App\Models\Goal;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @method \App\Models\Goal|\Illuminate\Support\Collection<\App\Models\Goal> create($attributes = [], ?Model $parent = null)
 * @method \Illuminate\Support\Collection<\App\Models\Goal> createMany(iterable $records)
 * @method \App\Models\Goal createOne($attributes = [])
 * @method \App\Models\Goal|\Illuminate\Support\Collection<\App\Models\Goal> make($attributes = [], ?Model $parent = null)
 * @method \App\Models\Goal makeOne($attributes = [])
 */
class GoalFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Goal::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => User::factory(),
            'name' => $this->faker->colorName,
            'image' => $this->faker->imageUrl(150, 200),
            'start_date' => $this->faker->dateTimeBetween('-1 month'),
            'end_date' => $this->faker->dateTimeBetween('now', '+1 month'),
            'unit' => UnitEnum::pages()->value,
            'pages' => $this->faker->numberBetween(50, 1000),
            'progress_pages' => function (array $attributes) {
                return $this->faker->numberBetween(1, $attributes['pages']);
            },
        ];
    }
}
