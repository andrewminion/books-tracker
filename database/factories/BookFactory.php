<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @method \App\Models\Book|\Illuminate\Support\Collection<\App\Models\Book> create($attributes = [], ?Model $parent = null)
 * @method \Illuminate\Support\Collection<\App\Models\Book> createMany(iterable $records)
 * @method \App\Models\Book createOne($attributes = [])
 * @method \App\Models\Book|\Illuminate\Support\Collection<\App\Models\Book> make($attributes = [], ?Model $parent = null)
 * @method \App\Models\Book makeOne($attributes = [])
 */
class BookFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'title' => $this->faker->words(8, true),
            'cover_image' => $this->faker->filePath(),
            'published_at' => $this->faker->date(),
            'description' => $this->faker->sentence(),
        ];
    }
}
