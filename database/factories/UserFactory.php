<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @method \App\Models\User|\Illuminate\Support\Collection<\App\Models\User> create($attributes = [], ?Model $parent = null)
 * @method \Illuminate\Support\Collection<\App\Models\User> createMany(iterable $records)
 * @method \App\Models\User createOne($attributes = [])
 * @method \App\Models\User|\Illuminate\Support\Collection<\App\Models\User> make($attributes = [], ?Model $parent = null)
 * @method \App\Models\User makeOne($attributes = [])
 */
class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name,
            'email' => $this->faker->unique()->safeEmail,
            'email_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'remember_token' => Str::random(10),
        ];
    }
}
