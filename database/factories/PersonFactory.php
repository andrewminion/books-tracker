<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @method \App\Models\Person|\Illuminate\Support\Collection<\App\Models\Person> create($attributes = [], ?Model $parent = null)
 * @method \Illuminate\Support\Collection<\App\Models\Person> createMany(iterable $records)
 * @method \App\Models\Person createOne($attributes = [])
 * @method \App\Models\Person|\Illuminate\Support\Collection<\App\Models\Person> make($attributes = [], ?Model $parent = null)
 * @method \App\Models\Person makeOne($attributes = [])
 */
class PersonFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $firstName = $this->faker->firstName();
        $lastName = $this->faker->lastName();

        return [
            'display_name' => $firstName.' '.$lastName,
            'sort_name' => $lastName.' '.$firstName,
        ];
    }
}
