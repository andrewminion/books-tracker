<?php

namespace Database\Factories;

use App\Models\Book;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @method \App\Models\UserBook|\Illuminate\Support\Collection<\App\Models\UserBook> create($attributes = [], ?Model $parent = null)
 * @method \Illuminate\Support\Collection<\App\Models\UserBook> createMany(iterable $records)
 * @method \App\Models\UserBook createOne($attributes = [])
 * @method \App\Models\UserBook|\Illuminate\Support\Collection<\App\Models\UserBook> make($attributes = [], ?Model $parent = null)
 * @method \App\Models\UserBook makeOne($attributes = [])
 */
class UserBookFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'book_id' => Book::factory(),
            'user_id' => User::factory(),
        ];
    }
}
