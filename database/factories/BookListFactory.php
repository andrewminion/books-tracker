<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @method \App\Models\BookList|\Illuminate\Support\Collection<\App\Models\BookList> create($attributes = [], ?Model $parent = null)
 * @method \Illuminate\Support\Collection<\App\Models\BookList> createMany(iterable $records)
 * @method \App\Models\BookList createOne($attributes = [])
 * @method \App\Models\BookList|\Illuminate\Support\Collection<\App\Models\BookList> make($attributes = [], ?Model $parent = null)
 * @method \App\Models\BookList makeOne($attributes = [])
 */
class BookListFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'user_id' => User::factory(),
            'name' => $this->faker->words(3, true),
            'description' => $this->faker->sentence(),
        ];
    }
}
