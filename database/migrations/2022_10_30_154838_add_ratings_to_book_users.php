<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('book_user', function (Blueprint $table) {
            $table->after('completed_at', function (Blueprint $table) {
                $table->tinyInteger('characters')->nullable();
                $table->tinyInteger('atmosphere')->nullable();
                $table->tinyInteger('writing_style')->nullable();
                $table->tinyInteger('plot')->nullable();
                $table->tinyInteger('intrigue')->nullable();
                $table->tinyInteger('logic')->nullable();
                $table->tinyInteger('enjoyment')->nullable();
                $table->text('notes')->nullable();
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('book_user', function (Blueprint $table) {
            $table->dropColumn([
                'characters',
                'atmosphere',
                'writing_style',
                'plot',
                'intrigue',
                'logic',
                'enjoyment',
                'notes',
            ]);
        });
    }
};
