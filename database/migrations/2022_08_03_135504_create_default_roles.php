<?php

use Illuminate\Database\Migrations\Migration;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /** @var Role $admin */
        $admin = Role::create(['name' => 'admin']);
        /** @var Role $user */
        $user = Role::create(['name' => 'user']);

        /** @var Permission $accessAdmin */
        $accessAdmin = Permission::create(['name' => 'access-admin']);

        $admin->givePermissionTo($accessAdmin);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        /** @var Role $admin */
        $admin = Role::firstWhere('name', 'admin');
        $admin->revokePermissionTo('access-admin');
    }
};
