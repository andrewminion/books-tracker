<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUnitsToGoals extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('goals', function (Blueprint $table) {
            $table->string('unit')->after('end_date')->nullable()->default('pages');

            $table->renameColumn('length', 'pages');
            $table->renameColumn('progress', 'progress_pages');

            $table->time('time')->nullable()->default(null);
            $table->time('progress_time')->nullable()->default(null);

            $table->float('percentage')->nullable()->default(null)->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('goals', function (Blueprint $table) {
            $table->dropColumn('unit');
            $table->dropColumn('percentage');
            $table->dropColumn('time');
            $table->dropColumn('progress_time');

            $table->renameColumn('pages', 'length');
            $table->renameColumn('progress_pages', 'progress');
        });
    }
}
