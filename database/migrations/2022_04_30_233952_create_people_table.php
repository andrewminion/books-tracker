<?php

use App\Models\Book;
use App\Models\Person;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('people', function (Blueprint $table) {
            $table->id();
            $table->string('display_name')->nullable();
            $table->string('sort_name')->nullable();
            $table->timestamps();
        });

        Schema::create('book_person', function (Blueprint $table) {
            $table->foreignIdFor(Book::class)->constrained();
            $table->foreignIdFor(Person::class)->constrained();
            $table->string('role');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('book_person');
        Schema::dropIfExists('people');
    }
};
