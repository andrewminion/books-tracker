<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('book_series', function (Blueprint $table) {
            $table->renameColumn('order', 'sort_order');
            $table->string('display_order')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('book_series', function (Blueprint $table) {
            $table->dropColumn('display_order');
            $table->renameColumn('sort_order', 'order');
        });
    }
};
