<?php

use App\Models\Book;
use App\Models\Person;
use App\Models\Series;
use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('series', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->text('description')->nullable();
            $table->string('cover_image')->nullable();
            $table->timestamps();
        });

        Schema::create('book_series', function (Blueprint $table) {
            $table->foreignIdFor(Book::class)->constrained();
            $table->foreignIdFor(Series::class)->constrained();
            $table->integer('order')->nullable();
        });

        Schema::create('person_series', function (Blueprint $table) {
            $table->foreignIdFor(Person::class)->constrained();
            $table->foreignIdFor(Series::class)->constrained();
            $table->string('role');
        });

        Schema::create('series_user', function (Blueprint $table) {
            $table->foreignIdFor(User::class)->constrained();
            $table->foreignIdFor(Series::class)->constrained();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('series_user');
        Schema::dropIfExists('person_series');
        Schema::dropIfExists('book_series');
        Schema::dropIfExists('series');
    }
};
